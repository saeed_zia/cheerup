package com.app.cheerthemup.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.cheerthemup.R;
import com.app.cheerthemup.interfaces.BlockUSerClickListener;
import com.app.cheerthemup.interfaces.ItemClickListener;
import com.app.cheerthemup.models.User;
import com.app.cheerthemup.utils.Constants;
import com.app.cheerthemup.views.activities.SigninActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UsersVH> {

    private ArrayList<User> users;

    private ArrayList<User> arraylist;
    private ItemClickListener itemClickListener;
    private BlockUSerClickListener mblockUSerClickListener;

    public UsersAdapter(ArrayList<User> users, BlockUSerClickListener blockUSerClickListener) {
        this.users = users;
        this.arraylist = new ArrayList<User>();
        this.arraylist.addAll(users);
         this.mblockUSerClickListener=blockUSerClickListener;
    }
    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
    @NonNull
    @Override
    public UsersVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item, parent, false);
        return new UsersVH(view, itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersVH holder, int position) {
        User user = users.get(position);

//        if (user.getEmail().equals(FirebaseAuth.getInstance().getCurrentUser().getEmail())) {

//        }else{


        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
// generate random color
        int color1 = generator.getRandomColor();
// generate color based on a key (same key returns the same color), useful for list/grid views
//        int color2 = generator.getColor("user@gmail.com")

        String userName = user.getUserName();
        //String firstTwoLetters = userName.substring(0, 2);


        TextDrawable textDrawable = TextDrawable.builder()
                .buildRoundRect(userName.substring(0, 1).toUpperCase(), color1, 48);
       // holder.imgUserProfile.setImageDrawable(textDrawable);

//        Random random = new Random();
//        int color = Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256));
//        TextDrawable drawable = TextDrawable.builder()
//                .buildRound(firstTwoLetters, Color.RED);

//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//        boolean userIdentity = sharedPreferences.getBoolean(Constants.USERIDENTITY,false);

        if (user.isPaidUser()) {
            if (user.isIdentityHidden()) {
                holder.txtName.setText("Anonymous");
                holder.surName.setText("SurName");
                holder.city.setText("City");
            } else {
                holder.txtName.setText(user.getName());
                holder.surName.setText(user.getSurName());
                holder.city.setText(user.getCity());
            }
        } else {

            holder.txtName.setText(user.getName());
            holder.surName.setText(user.getSurName());
            holder.city.setText(user.getCity());
        }

        holder.optionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(view.getContext(), holder.optionMenu);
                //inflating menu from xml resource
                popup.getMenu().add(Menu.NONE, 0, 0, "Block"); //parm 2 is menu id, param 3 is position of this menu item in menu items list, param 4 is title of the menu
                popup.getMenu().add(Menu.NONE, 1, 1, "Unblock");
                //popup.inflate(R.menu.menu_main);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case 0:
                                mblockUSerClickListener.onBlockUSerClickListener(position);
                                //handle menu1 click
                                return true;
                            case 1:
                                mblockUSerClickListener.onUnBlockUSerClickListener(position);
                                //handle menu1 click
                                return true;

                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();
            }
        });

    try {
        Picasso.get()
                .load(user.getImage_url())
                .fit()
                .centerCrop()
                .into(holder.imgUserProfile);
    }catch (Exception e){
        e.printStackTrace();
    }

//        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public static class UsersVH extends RecyclerView.ViewHolder {


        ImageView imgUserProfile,optionMenu;
        TextView txtName,surName,city;

        public UsersVH(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);

            imgUserProfile = itemView.findViewById(R.id.img_user_profile);
            txtName = itemView.findViewById(R.id.name);
            surName = itemView.findViewById(R.id.surname);
            city = itemView.findViewById(R.id.city);
            imgUserProfile=itemView.findViewById(R.id.user_image);
            optionMenu=itemView.findViewById(R.id.option_menu);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    itemClickListener.onItemClick(getAdapterPosition(),txtName.getText().toString());
                }
            });


        }
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        users.clear();
        if (charText.length() == 0) {
            users.addAll(arraylist);
        } else {
            for (User wp : arraylist) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    users.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
