package com.app.cheerthemup.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

public  class Dialogs {
    ProgressDialog progressDialog;
    public void showProgressDialogWithTitle(Context context, String title) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setMessage(title);
        progressDialog.show();
    }
    public  void hideDialog()
    {
        progressDialog.dismiss();
    }
}
