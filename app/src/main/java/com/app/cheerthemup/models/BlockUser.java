package com.app.cheerthemup.models;

public class BlockUser {

    boolean blockStatus;
    String blockerId;
    public BlockUser(String blockerId, boolean blockStatus) {
        this.blockerId = blockerId;
        this.blockStatus = blockStatus;
    }

    public boolean isBlockStatus() {
        return blockStatus;
    }

    public void setBlockStatus(boolean blockStatus) {
        this.blockStatus = blockStatus;
    }

    public BlockUser() {
    }

    public String getBlockerId() {
        return blockerId;
    }

    public void setBlockerId(String blockerId) {
        this.blockerId = blockerId;
    }
}
