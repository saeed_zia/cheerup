package com.app.cheerthemup.views.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.cheerthemup.interfaces.BlockUSerClickListener;
import com.app.cheerthemup.models.BlockUser;
import com.app.cheerthemup.models.Message;
import com.app.cheerthemup.utils.Dialogs;
import com.app.cheerthemup.views.activities.MessageActivity1;
import com.app.cheerthemup.R;
import com.app.cheerthemup.interfaces.ItemClickListener;
import com.app.cheerthemup.models.User;
import com.app.cheerthemup.adapters.UsersAdapter;
import com.app.cheerthemup.notifications.Token;
import com.app.cheerthemup.utils.Constants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;


public class UsersFragment extends Fragment implements SearchView.OnQueryTextListener,BlockUSerClickListener {
    private static final String TAG = "ProfileFragment";

    BlockUSerClickListener blockUSerClickListener;
    //views
    RecyclerView recyclerView;
    SearchView editsearch;

    //adapter
    UsersAdapter usersAdapter;
    ArrayList<User> users;

    //firebase
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;

    TextView txtToolbarTitle;
    Dialogs dialogs=new Dialogs();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_users, container, false);

        txtToolbarTitle = getActivity().findViewById(R.id.txt_toolbar_title);

        txtToolbarTitle.setText("Users");


        users = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerview_users);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        recyclerView.setHasFixedSize(true);


      dialogs.showProgressDialogWithTitle(getContext(),"Loading User Data");
        usersAdapter = new UsersAdapter(users,this);
        databaseReference = FirebaseDatabase.getInstance().getReference("Users");

        editsearch = (SearchView)view.findViewById(R.id.searchView);
        editsearch.setOnQueryTextListener(this);
        getUsersData();
        updateToken(FirebaseInstanceId.getInstance().getToken());
        return view;
    }

    private void updateToken(String token) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Tokens");

        Token token1 = new Token(token);
        reference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(token1);


    }

    private void getUsersData() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                users.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    User user = dataSnapshot.getValue(User.class);
                    if (user.getEmail().equals(FirebaseAuth.getInstance().getCurrentUser().getEmail())) {

                    } else {

                        users.add(user);
                    }
                }
                dialogs.hideDialog();
                 usersAdapter.notifyDataSetChanged();
                recyclerView.setAdapter(usersAdapter);

                usersAdapter.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onItemClick(int pos, String userName) {

                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                        SharedPreferences.Editor edit = prefs.edit();


//                        boolean recieverIdentityHidden = users.get(pos).isIdentityHidden();
                        Intent intent = new Intent(getActivity(), MessageActivity1.class);
                        String uid = users.get(pos).getUid();

//                        edit.putString(Constants.USERNAME, userName);
//                        edit.putString(Constants.fromNotification, "false");
                        intent.putExtra(Constants.USERNAME, userName);
                        intent.putExtra(Constants.UID, uid);
                        intent.putExtra(Constants.fromNotification, "false");
                        checkUserBlockStatus(uid,intent);
                        //for not from notification
//                        intent.putExtra(Constants.RECIEVERIDENTITYHIDDEN, String.valueOf(recieverIdentityHidden));
                        //for from notification
//                        edit.putString(Constants.RECIEVERIDENTITYHIDDEN, String.valueOf(recieverIdentityHidden));
//                        edit.apply();
                        //startActivity(intent);
                    }
                });
                Log.d(TAG, "onDataChange: " + users);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: Profile");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach: Profile");
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        String text = s;
        usersAdapter.filter(text);
        return false;
    }
    public void blockUser(String uid){

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        DatabaseReference chatDatabaseReference = FirebaseDatabase.getInstance().getReference("Chat");
        String ref1=firebaseAuth.getCurrentUser().getUid();
        String ref2=uid;


        BlockUser blockUser=new BlockUser(ref1,true);
        if(ref1.compareTo(ref2)<0){
            chatDatabaseReference.child(ref1).child(ref1 + "_" + ref2).child("BlockStatus").setValue(blockUser);
        }
        else{
            chatDatabaseReference.child(ref2).child(ref2 + "_" + ref1).child("BlockStatus").setValue(blockUser);
        }
        Toast.makeText(getContext(), "Blocked", Toast.LENGTH_LONG).show();
            //chatDatabaseReference.child(firebaseAuth.getCurrentUser().getUid()).child(firebaseAuth.getCurrentUser().getUid() + "_" + reciverId).child("Msg").setValue(message);
           // Log.d(TAG, "sendMessage:1------------- " + firebaseAuth.getCurrentUser().getUid() + "." + firebaseAuth.getCurrentUser().getUid() + "_" + reciverId);
            //  chatDatabaseReference.child(reciverId).child(reciverId + "_" + firebaseAuth.getCurrentUser().getUid()).child("Msg").setValue(message);
           // Log.d(TAG, "sendMessage:2------------- " + reciverId + "." + reciverId + "_" + firebaseAuth.getCurrentUser().getUid());
    }
    public void unBlockUser(String uid){

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        DatabaseReference chatDatabaseReference = FirebaseDatabase.getInstance().getReference("Chat");
        String ref1=firebaseAuth.getCurrentUser().getUid();
        String ref2=uid;


        BlockUser blockUser=new BlockUser(ref1,false);
        if(ref1.compareTo(ref2)<0){
            chatDatabaseReference.child(ref1).child(ref1 + "_" + ref2).child("BlockStatus").setValue(blockUser);
        }
        else{
            chatDatabaseReference.child(ref2).child(ref2 + "_" + ref1).child("BlockStatus").setValue(blockUser);
        }
        Toast.makeText(getContext(), "UnBlocked", Toast.LENGTH_LONG).show();
        //chatDatabaseReference.child(firebaseAuth.getCurrentUser().getUid()).child(firebaseAuth.getCurrentUser().getUid() + "_" + reciverId).child("Msg").setValue(message);
        // Log.d(TAG, "sendMessage:1------------- " + firebaseAuth.getCurrentUser().getUid() + "." + firebaseAuth.getCurrentUser().getUid() + "_" + reciverId);
        //  chatDatabaseReference.child(reciverId).child(reciverId + "_" + firebaseAuth.getCurrentUser().getUid()).child("Msg").setValue(message);
        // Log.d(TAG, "sendMessage:2------------- " + reciverId + "." + reciverId + "_" + firebaseAuth.getCurrentUser().getUid());
    }
    public void  checkUserBlockStatus(String receiverUid,Intent intent){
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        DatabaseReference chatDatabaseReference = FirebaseDatabase.getInstance().getReference("Chat");
        String ref1=firebaseAuth.getCurrentUser().getUid();
        String ref2=receiverUid;
        DatabaseReference msg = null;
         boolean message;
        if(ref1.compareTo(ref2)<0) {
            msg = chatDatabaseReference.child(ref1).child(ref1 + "_" + ref2).child("BlockStatus");
        }
        else{
            msg = chatDatabaseReference.child(ref2).child(ref2 + "_" + ref1).child("BlockStatus");
        }

        // DatabaseReference msg = chatDatabaseReference.child(firebaseAuth.getCurrentUser().getUid()).child(firebaseAuth.getCurrentUser().getUid() + "_" + reciverId).child("Msg");
        msg.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                    if (snapshot.getValue() == null) {
                        startActivity(intent);
                    }
                    else {
                        BlockUser blockUser = snapshot.getValue(BlockUser.class);
                        if (blockUser.isBlockStatus()) {
                                if(blockUser.getBlockerId().equals(ref1)) {
                                    Toast.makeText(getContext(), "User is blocked", Toast.LENGTH_LONG).show();
                                }
                                else{
                                    Toast.makeText(getContext(), "User has blocked you", Toast.LENGTH_LONG).show();
                                }
                        }
                        else {
                            startActivity(intent);
                        }


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public void onBlockUSerClickListener(int pos) {
        User user=users.get(pos);
        blockUser(user.getUid());
    }

    @Override
    public void onUnBlockUSerClickListener(int pos) {
        User user=users.get(pos);
        unBlockUser(user.getUid());
    }

}