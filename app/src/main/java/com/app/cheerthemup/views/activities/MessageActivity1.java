package com.app.cheerthemup.views.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.app.cheerthemup.BuildConfig;
import com.app.cheerthemup.R;
import com.app.cheerthemup.models.Message;
import com.app.cheerthemup.models.User;
import com.app.cheerthemup.notifications.Client;
import com.app.cheerthemup.notifications.Data;
import com.app.cheerthemup.notifications.MyResponse;
import com.app.cheerthemup.notifications.Sender;
import com.app.cheerthemup.notifications.Token;
import com.app.cheerthemup.notifications.api.APIService;
import com.app.cheerthemup.utils.Constants;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.annotations.NotNull;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static com.android.billingclient.api.BillingClient.SkuType.INAPP;
public class MessageActivity1 extends AppCompatActivity implements PurchasesUpdatedListener{


    private static final String TAG = "MessageActivity1";
    MaterialButton btnSend, btnUpgradeToPremium;
    TextInputEditText edtMsg;
    TextView txtMsgLeft,sender;
    Toolbar toolbar;
    LinearLayout linPremium;
    RelativeLayout relativeLayoutMsg;
    ArrayList items;


    //Firebase
    FirebaseAuth firebaseAuth;

    //fcm api
    public static final String BASE_URL = "https://fcm.googleapis.com/";
    APIService apiService;
    boolean notify = false;


    SharedPreferences prefs;
    SharedPreferences.Editor edit;
//
private BillingClient billingClient;
    public static final String PREF_FILE= "MyPref";
    public static final String PURCHASE_KEY= "purchase";
    public static final String PRODUCT_ID= "cheerthemup";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message1);

        sender=findViewById(R.id.sender);
        // Establish connection to billing client
        //check purchase status from google play store cache
        //to check if item already Purchased previously or refunded
        btnUpgradeToPremium = findViewById(R.id.btn_upgrade_to_premium);
        billingClient = BillingClient.newBuilder(this)
                .enablePendingPurchases().setListener(this).build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if(billingResult.getResponseCode()==BillingClient.BillingResponseCode.OK){
                    Purchase.PurchasesResult queryPurchase = billingClient.queryPurchases(INAPP);
                    List<Purchase> queryPurchases = queryPurchase.getPurchasesList();
                    if(queryPurchases!=null && queryPurchases.size()>0){
                        handlePurchases(queryPurchases);
                    }
                    //if purchase list is empty that means item is not purchased
                    //Or purchase is refunded or canceled
                    else{
                        savePurchaseValueToPref(false);
                    }
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
            }
        });

        //item Purchased
        if(getPurchaseValueFromPref()){
          //  btnUpgradeToPremium.setVisibility(View.GONE);
            //purchaseStatus.setText("Purchase Status : Purchased");
        }
        //item not Purchased
        else{
         //   btnUpgradeToPremium.setVisibility(View.VISIBLE);
       //     purchaseStatus.setText("Purchase Status : Not Purchased");
        }

        ArrayList items = new ArrayList();


//        ArrayAdapter arrayAdapter =new ArrayAdapter(this,R.layout.list_item, items);
//        AutoCompleteTextView autoCompleteTextView=findViewById(R.id.autoComplete);
//        autoCompleteTextView.setAdapter(arrayAdapter);
//        TextInputLayout textInputLayout=findViewById(R.id.menuInputLayout);
//        ((AutoCompleteTextView)textInputLayout.getEditText()).setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                String selectedValue = arrayAdapter.getItem(position).toString();
//                edtMsg.setText(selectedValue);
//            }
//        });

            List<String> nonConsumablesList = Collections.singletonList("lifetime");
            List<String> consumablesList = Arrays.asList("base", "moderate", "quite", "plenty", "yearly");
            List<String> subsList = Collections.singletonList("subscription");


        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        edit = prefs.edit();

        apiService = Client.getClient(BASE_URL).create(APIService.class);

        relativeLayoutMsg = findViewById(R.id.rel_msg);
        linPremium = findViewById(R.id.lin_premium);
        firebaseAuth = FirebaseAuth.getInstance();
        toolbar = findViewById(R.id.toolbar);


        btnSend = findViewById(R.id.btn_send);
        edtMsg = findViewById(R.id.edit_msg);
        txtMsgLeft = findViewById(R.id.txt_msg_left);
     //   txtMsgRight = findViewById(R.id.txt_msg_right);
        txtMsgLeft.setMovementMethod(new ScrollingMovementMethod());
     //   txtMsgRight.setMovementMethod(new ScrollingMovementMethod());
        String notiExtra = getIntent().getStringExtra(Constants.fromNotification);


//        if (prefs.getString(Constants.fromNotification, "").equals("true")) {
        if (notiExtra.equals("true")) {
            //////////////////////////////////////FROM NOTIIFICATION
            Log.d(TAG, "onCreate: noti extra true");

            String userName = getIntent().getStringExtra(Constants.USERNAME);
            String senderId = getIntent().getStringExtra(Constants.UID);
            String userStatus = prefs.getString(Constants.USERSTATUS, "");

            String[] split = userName.split(":");
            setMessage(senderId, split[0],split[1]); //sender id,sender name,msg
           // getMsgFromFirebase(recieverUid, txtMsgLeft);
            checkUserStatus(userStatus);

            btnUpgradeToPremium.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                      purchase();

                }
            });

            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notify = true;
                    String msg = edtMsg.getText().toString();
                    edtMsg.setText("");
                    sendMessage(msg, senderId); //now sender has become receiver
                    //
//                    txtMsgRight.setVisibility(View.VISIBLE);
//                    txtMsgLeft.setVisibility(View.INVISIBLE);
                       sender.setText("You:");
//                    txtMsgRight.setText(msg);


                }
            });


        }
        else {
            //////////////////////////////////////NOT FROM NOTIIFICATION
            Log.d(TAG, "onCreate: noti extra false");

            String otherUserName = getIntent().getStringExtra(Constants.USERNAME);
            String otherUserId = getIntent().getStringExtra(Constants.UID);
            String otherUserStatus = prefs.getString(Constants.USERSTATUS, "");

           // setUserNameOnToolbar(recieverUid, userName);
            getMsgFromFirebase(otherUserId,otherUserName);
            checkUserStatus(otherUserStatus);


            btnUpgradeToPremium.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    purchase();
                }
            });
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  onBackPressed();
                }
            });
            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notify = true;
                    String msg = edtMsg.getText().toString();
                    edtMsg.setText("");
                    sendMessage(msg, otherUserId);
//                    txtMsgRight.setVisibility(View.VISIBLE);
//                    txtMsgLeft.setVisibility(View.INVISIBLE);
                       sender.setText("You:");
//                    txtMsgRight.setText(msg);

                }
            });

        }


    }

/////
private SharedPreferences getPreferenceObject() {
    return getApplicationContext().getSharedPreferences(PREF_FILE, 0);
}
    private SharedPreferences.Editor getPreferenceEditObject() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(PREF_FILE, 0);
        return pref.edit();
    }
    private boolean getPurchaseValueFromPref(){
        return getPreferenceObject().getBoolean( PURCHASE_KEY,false);
    }
    private void savePurchaseValueToPref(boolean value){
        getPreferenceEditObject().putBoolean(PURCHASE_KEY,value).commit();
    }


    //initiate purchase on button click
    public void purchase() {
        //check if service is already connected
        if (billingClient.isReady()) {
            initiatePurchase();
        }
        //else reconnect service
        else{
            billingClient = BillingClient.newBuilder(this).enablePendingPurchases().setListener(this).build();
            billingClient.startConnection(new BillingClientStateListener() {
                @Override
                public void onBillingSetupFinished(BillingResult billingResult) {
                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                        initiatePurchase();
                    } else {
                        Toast.makeText(getApplicationContext(),"Error "+billingResult.getDebugMessage(),Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onBillingServiceDisconnected() {
                }
            });
        }
    }
    private void initiatePurchase() {
        List<String> skuList = new ArrayList<>();
        skuList.add(PRODUCT_ID);
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(INAPP);
        billingClient.querySkuDetailsAsync(params.build(),
                new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(BillingResult billingResult,
                                                     List<SkuDetails> skuDetailsList) {
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                            if (skuDetailsList != null && skuDetailsList.size() > 0) {
                                BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                                        .setSkuDetails(skuDetailsList.get(0))
                                        .build();
                                billingClient.launchBillingFlow(MessageActivity1.this, flowParams);
                            }
                            else{
                                //try to add item/product id "purchase" inside managed product in google play console
                                Toast.makeText(getApplicationContext(),"Purchase Item not Found",Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    " Error "+billingResult.getDebugMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    private void sendMessage(String msg, String reciverId) {

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        DatabaseReference chatDatabaseReference = FirebaseDatabase.getInstance().getReference("Chat");


        Message message = new Message(msg);
        String ref1=firebaseAuth.getCurrentUser().getUid();
        String ref2=reciverId;
        if(ref1.compareTo(ref2)<0){
            chatDatabaseReference.child(ref1).child(ref1 + "_" + ref2).child("Msg").setValue(message);
        }
        else{
            chatDatabaseReference.child(ref2).child(ref2 + "_" + ref1).child("Msg").setValue(message);
        }
        //chatDatabaseReference.child(firebaseAuth.getCurrentUser().getUid()).child(firebaseAuth.getCurrentUser().getUid() + "_" + reciverId).child("Msg").setValue(message);
        Log.d(TAG, "sendMessage:1------------- " + firebaseAuth.getCurrentUser().getUid() + "." + firebaseAuth.getCurrentUser().getUid() + "_" + reciverId);
      //  chatDatabaseReference.child(reciverId).child(reciverId + "_" + firebaseAuth.getCurrentUser().getUid()).child("Msg").setValue(message);
        Log.d(TAG, "sendMessage:2------------- " + reciverId + "." + reciverId + "_" + firebaseAuth.getCurrentUser().getUid());

        final String mesage = msg;
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(firebaseAuth.getCurrentUser().getUid());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user = snapshot.getValue(User.class);
                if (notify) {
                    sendNotification(reciverId, user.getUserName(), mesage);
                }
                notify = false;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    private void sendNotification(String reciverId, String userName, String mesage) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MessageActivity1.this);
        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference("Tokens");
        Query query = tokens.orderByKey().equalTo(reciverId);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {

                    Token token = dataSnapshot.getValue(Token.class);
                    Data data;
                    boolean userIdentity = sharedPreferences.getBoolean(Constants.USERIDENTITY, false);
                    if (userIdentity) {
                        data = new Data(firebaseAuth.getCurrentUser().getUid(), R.drawable.ic_baseline_notifications_24, "Anonymous" + ": " + mesage, "Someone sent you a compliment", reciverId);
                    } else {
                        data = new Data(firebaseAuth.getCurrentUser().getUid(), R.drawable.ic_baseline_notifications_24, userName + ": " + mesage, "Someone sent you a compliment", reciverId);

                    }
                    Sender sender = new Sender(data, token.getToken());
                    apiService.sendNotification(sender)
                            .enqueue(new Callback<MyResponse>() {
                                @Override
                                public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                    Log.d(TAG, "onResponse: " + response);
                                    if (response.code() == 200) {
                                        if (response.body().success != 1) {
                                            Toast.makeText(MessageActivity1.this, "failed", Toast.LENGTH_SHORT).show();
                                        }
                                        Log.d(TAG, "onResponse: ");
                                    }
                                }
                                @Override
                                public void onFailure(Call<MyResponse> call, Throwable t) {
                                    Log.d(TAG, "onFailure: ");
                                    Toast.makeText(MessageActivity1.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();

                                }
                            });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void getMsgFromFirebase(String reciverId,String receiverName ) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        DatabaseReference chatDatabaseReference = FirebaseDatabase.getInstance().getReference("Chat");
     String ref1=firebaseAuth.getCurrentUser().getUid();
     String ref2=reciverId;
        DatabaseReference msg = null;
      if(ref1.compareTo(ref2)<0) {
          msg = chatDatabaseReference.child(ref1).child(ref1 + "_" + ref2).child("Msg");
      }
      else{
          msg = chatDatabaseReference.child(ref2).child(ref2 + "_" + ref1).child("Msg");
      }

          // DatabaseReference msg = chatDatabaseReference.child(firebaseAuth.getCurrentUser().getUid()).child(firebaseAuth.getCurrentUser().getUid() + "_" + reciverId).child("Msg");
          msg.addValueEventListener(new ValueEventListener() {
              @Override
              public void onDataChange(@NonNull DataSnapshot snapshot) {
                  if (snapshot.getValue() == null) {
                      txtMsgLeft.setText("");
                  }
                  else {

                      Message message = snapshot.getValue(Message.class);

                      setMessage(reciverId,receiverName,message.getMesssage());
                     //  setUserNameOnToolbar(reciverId,message.getSender());
//                      txtMsgRight.setVisibility(View.INVISIBLE);
//                      txtMsgLeft.setVisibility(View.VISIBLE);

                  }
              }

              @Override
              public void onCancelled(@NonNull DatabaseError error) {

              }
          });

    }

    private void setMessage(String otherUserId, String otherUserName,String msg) {
        DatabaseReference users = FirebaseDatabase.getInstance().getReference("Users").child(otherUserId);
        users.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user = snapshot.getValue(User.class);

                txtMsgLeft.setText(msg);
                if (user.isIdentityHidden()) {
                   // toolbar.setTitle("Sender:"+user.getAnonymousUser());
                    sender.setText("Anynomous:");
                } else {
                   sender.setText(otherUserName+":");

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void upgradeToPremium() {
        DatabaseReference users = FirebaseDatabase.getInstance().getReference("Users").child(firebaseAuth.getCurrentUser().getUid());
        users.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                User user = snapshot.getValue(User.class);
                user.setPaidUser(true);
                edit.putString(Constants.USERSTATUS, String.valueOf(user.isPaidUser()));
                users.setValue(user);
                linPremium.setVisibility(View.GONE);

                Snackbar.make(relativeLayoutMsg, "Upgraded to premium", Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    private void checkUserStatus(String userStatus) {
        if (userStatus.equals("true")) {
            linPremium.setVisibility(View.GONE);
            //edtMsg.setEnabled(true);
        } else {
            linPremium.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> purchases) {
        //if item newly purchased
        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && purchases != null) {
            handlePurchases(purchases);
        }
        //if item already purchased then check and reflect changes
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
            Purchase.PurchasesResult queryAlreadyPurchasesResult = billingClient.queryPurchases(INAPP);
            List<Purchase> alreadyPurchases = queryAlreadyPurchasesResult.getPurchasesList();
            if(alreadyPurchases!=null){
                handlePurchases(alreadyPurchases);
            }
        }
        //if purchase cancelled
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
            Toast.makeText(getApplicationContext(),"Purchase Canceled",Toast.LENGTH_SHORT).show();
        }
        // Handle any other error msgs
        else {
            Toast.makeText(getApplicationContext(),"Error "+billingResult.getDebugMessage(),Toast.LENGTH_SHORT).show();
        }
    }
    void handlePurchases(List<Purchase>  purchases) {
        for(Purchase purchase:purchases) {
            //if item is purchased
            if (PRODUCT_ID.equals(purchase.getSkus()) && purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED)
            {
                if (!verifyValidSignature(purchase.getOriginalJson(), purchase.getSignature())) {
                    // Invalid purchase
                    // show error to user
                    Toast.makeText(getApplicationContext(), "Error : Invalid Purchase", Toast.LENGTH_SHORT).show();
                    return;
                }
                // else purchase is valid
                //if item is purchased and not acknowledged
                if (!purchase.isAcknowledged()) {
                    AcknowledgePurchaseParams acknowledgePurchaseParams =
                            AcknowledgePurchaseParams.newBuilder()
                                    .setPurchaseToken(purchase.getPurchaseToken())
                                    .build();
                    billingClient.acknowledgePurchase(acknowledgePurchaseParams, ackPurchase);
                }
                //else item is purchased and also acknowledged
                else {
                    // Grant entitlement to the user on item purchase
                    // restart activity
                    if(!getPurchaseValueFromPref()){
                        savePurchaseValueToPref(true);
                        Toast.makeText(getApplicationContext(), "Item Purchased", Toast.LENGTH_SHORT).show();
                        this.recreate();
                    }
                }
            }
            //if purchase is pending
            else if( PRODUCT_ID.equals(purchase.getSkus()) && purchase.getPurchaseState() == Purchase.PurchaseState.PENDING)
            {
                Toast.makeText(getApplicationContext(),
                        "Purchase is Pending. Please complete Transaction", Toast.LENGTH_SHORT).show();
            }
            //if purchase is unknown
            else if(PRODUCT_ID.equals(purchase.getSkus()) && purchase.getPurchaseState() == Purchase.PurchaseState.UNSPECIFIED_STATE)
            {
                savePurchaseValueToPref(false);
             //   purchaseStatus.setText("Purchase Status : Not Purchased");
             //
           //     btnUpgradeToPremium.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "Purchase Status Unknown", Toast.LENGTH_SHORT).show();
            }
        }
    }
    AcknowledgePurchaseResponseListener ackPurchase = new AcknowledgePurchaseResponseListener() {
        @Override
        public void onAcknowledgePurchaseResponse(BillingResult billingResult) {
            if(billingResult.getResponseCode()==BillingClient.BillingResponseCode.OK){
                //if purchase is acknowledged
                // Grant entitlement to the user. and restart activity
                savePurchaseValueToPref(true);
                upgradeToPremium();
                Toast.makeText(getApplicationContext(), "Item Purchased", Toast.LENGTH_SHORT).show();
                MessageActivity1.this.recreate();
            }
        }
    };

    /**
     * Verifies that the purchase was signed correctly for this developer's public key.
     * <p>Note: It's strongly recommended to perform such check on your backend since hackers can
     * replace this method with "constant true" if they decompile/rebuild your app.
     * </p>
     */
    private boolean verifyValidSignature(String signedData, String signature) {
        try {
            // To get key go to Developer Console > Select your app > Development Tools > Services & APIs.
            String base64Key=getString(R.string.license_key);
            return Security.verifyPurchase(base64Key, signedData, signature);
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(billingClient!=null){
            billingClient.endConnection();
        }
    }
}