package com.app.cheerthemup.views.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.cheerthemup.R;
import com.app.cheerthemup.models.User;
import com.app.cheerthemup.utils.Constants;
import com.darwin.viola.still.FaceDetectionListener;
import com.darwin.viola.still.Viola;
import com.darwin.viola.still.model.FaceDetectionError;
import com.darwin.viola.still.model.Result;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.annotations.NotNull;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.InputStream;

public class ProfileFragment extends Fragment {
    private static final String TAG = "ProfileFragment";

    //views
    TextView txtToolbarTitle, txtUserEmail, txtUserPass, txtUserPhone, txtUserName,txtName,txtSurname,txtCity;
    CheckBox checkBoxIdentityShowHidden;
    LinearLayout linearLayoutIdentity;
    //firebase
    DatabaseReference databaseReference;

    SharedPreferences prefs;
    Bitmap bitmap;
    ProgressDialog progressDialog;
    SharedPreferences.Editor edit;
    ImageView profile;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        txtUserEmail = view.findViewById(R.id.txt_user_email);
        txtUserPhone = view.findViewById(R.id.txt_user_phone);
        txtUserName = view.findViewById(R.id.txt_user_name);
        txtName=view.findViewById(R.id.txt_name);
        txtSurname=view.findViewById(R.id.txt_surname);
        txtCity=view.findViewById(R.id.txt_user_city);
        progressDialog = new ProgressDialog(getContext());


        checkBoxIdentityShowHidden = view.findViewById(R.id.checkbox_hide_show_identity);
        linearLayoutIdentity = view.findViewById(R.id.lin_identity);
        profile=view.findViewById(R.id.img_profile);
        txtToolbarTitle = getActivity().findViewById(R.id.txt_toolbar_title);

        txtToolbarTitle.setText("Profile");

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        edit = prefs.edit();
        String uid = prefs.getString(Constants.UID, "");


        databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(uid);


        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Log.d(TAG, "onDataChange: " + snapshot);
                User user = snapshot.getValue(User.class);

                txtUserEmail.setText(user.getEmail());
                txtUserName.setText(user.getUserName());
                txtUserPhone.setText(user.getPhone());
                txtName.setText(user.getName());
                txtSurname.setText(user.getSurName());
                txtCity.setText(user.getCity());

                try {
                    Picasso.get()
                            .load(user.getImage_url())
                             .placeholder(R.drawable.ic_profile)
                            .fit()
                            .centerCrop()

                            .into(profile, new Callback() {
                                @Override
                                public void onSuccess() {
                                    progressDialog.dismiss();
                                }

                                @Override
                                public void onError(Exception e) {
                                    e.printStackTrace();
                                }
                            });
                }catch (Exception e){
                    e.printStackTrace();
                }

                if (user.isPaidUser()) {
                    linearLayoutIdentity.setVisibility(View.VISIBLE);
                    if (user.isIdentityHidden()) {
                        checkBoxIdentityShowHidden.setChecked(true);
                        edit.putBoolean(Constants.USERIDENTITY, true);
                        edit.apply();
                    } else {
                        checkBoxIdentityShowHidden.setChecked(false);
                        edit.putBoolean(Constants.USERIDENTITY, false);
                        edit.apply();
                    }
                } else {
                    linearLayoutIdentity.setVisibility(View.GONE);
                }
//                checkBoxIdentityShowHidden.setChecked(user.isPaidUser()? user.isIdentityHidden());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
///
         final FaceDetectionListener listener = new FaceDetectionListener() {
            @Override
            public void onFaceDetected(@NotNull Result result) {
               // Toast.makeText(getContext(), "Detected", Toast.LENGTH_SHORT).show();
                profile.setImageBitmap(bitmap);

            }

            @Override
            public void onFaceDetectionFailed(@NotNull FaceDetectionError error, @NotNull String message) {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        };
        ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            // There are no request codes
                            Intent data = result.getData();
                            try {
                                InputStream inputStream =getContext().getContentResolver().openInputStream(data.getData());

                                Uri imageUri = data.getData();
                                 bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), imageUri);

                                Viola viola = new Viola(listener);
                               // viola.addAgeClassificationPlugin(context) //optional, available via external dependency
                                viola.detectFace(bitmap);

                            }
                            catch (Exception e){
                                Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                someActivityResultLauncher.launch(Intent.createChooser(intent, "Select Picture"));

            }
        });
        ///
        checkBoxIdentityShowHidden.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d(TAG, "onCheckedChanged: ");
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        User user = snapshot.getValue(User.class);
                        if (isChecked) {
                            user.setIdentityHidden(true);
                            edit.putBoolean(Constants.USERIDENTITY, true);
                            edit.apply();
                            databaseReference.setValue(user);
                        } else {
                            user.setIdentityHidden(false);
                            edit.putBoolean(Constants.USERIDENTITY, false);
                            edit.apply();
                            databaseReference.setValue(user);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });

        return view;
    }


    private void showProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading your profile");
        progressDialog.show();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: Profile");
    }
    @Override
    public void onDetach() {
        super.onDetach();
        progressDialog.dismiss();
        Log.d(TAG, "onDetach: Profile");
    }
}