package com.app.cheerthemup.views.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.app.cheerthemup.R

class ThankYou : AppCompatActivity() {
    var tv_rating:TextView?=null
    var ok:Button?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_thank_you)

        tv_rating=findViewById(R.id.tv_rating);
        ok=findViewById(R.id.ok);
        var rating=intent.getFloatExtra("rating",1f)
       // tv_rating!!.setText("rating")

        ok!!.setOnClickListener{
            finish()
           startActivity(Intent(this,DashboardActivity::class.java))
        }
    }
}