package com.app.cheerthemup.views.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.app.cheerthemup.R;
import com.app.cheerthemup.utils.Dialogs;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

public class SignupActivity extends AppCompatActivity {

    MaterialTextView txtSignin;
    MaterialButton btnSignup;
    TextInputEditText edtUsername, edtEmail, edtPassword, edtPhone;

    //firebase
    FirebaseAuth firebaseAuth;
    DatabaseReference reference;
    FirebaseAuth.AuthStateListener authStateListener;
    Dialogs dialogs=new Dialogs();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);


        firebaseAuth = FirebaseAuth.getInstance();


//        edtUsername = findViewById(R.id.edit_username);
        edtEmail = findViewById(R.id.edit_email);
        edtPassword = findViewById(R.id.edit_password);
//        edtPhone = findViewById(R.id.edit_phone);

        btnSignup = findViewById(R.id.btn_signup);
        txtSignin = findViewById(R.id.txt_signin);

        txtSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignupActivity.this, SigninActivity.class));
            }
        });


        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String userName = edtUsername.getText().toString();
                String email = edtEmail.getText().toString();
                String password = edtPassword.getText().toString();
//                String phone = edtPhone.getText().toString();
//                verifyEmail(email,password,phone,userName);
                if (isNetworkConnected()) {
                    dialogs.showProgressDialogWithTitle(SignupActivity.this,"Please wait ...");
                    register(email ,password);
                }
                else{
                    Toast.makeText(SignupActivity.this, "Please Check you internet connection", Toast.LENGTH_LONG).show();
                }

            }
        });


    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
    private void register(String email, String password) {
//        progressBar.setVisibility(View.VISIBLE);


        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        dialogs.hideDialog();
                        if (task.isSuccessful()) {
                            FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                            verifyEmail(email,password);
//                            authStateListener = new FirebaseAuth.AuthStateListener() {
//                                @Override
//                                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                                    FirebaseUser currentUser = firebaseAuth.getCurrentUser();
//                                    if (currentUser != null) {
//                                        verifyEmail();
//                                    }else{
//                                        //user is signet out
//
//                                    }
//                                }
//                            };


                        } else {
                            Toast.makeText(SignupActivity.this, "Error : " + task.getException(), Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(SignupActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void verifyEmail(String email,String password) {

        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        currentUser.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            //Toast.makeText(SignupActivity.this, "Please verify your email", Toast.LENGTH_SHORT).show();
                            showAlertDialog();

                        }else{
                            //email not sent
                            //Toast.makeText(SignupActivity.this, "Email not Found", Toast.LENGTH_SHORT).show();
                            new AlertDialog.Builder(getApplicationContext())
                                    .setTitle("Email not Found")
                                    .setMessage("Your email address not found,Please enter it again ")
                                    // Specifying a listener allows you to take an action before dismissing the dialog.
                                    // The dialog is automatically dismissed when a dialog button is clicked.
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    })

                                    // A null listener allows the button to dismiss the dialog and take no further action.

                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();



                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(SignupActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }
    public void showAlertDialog(){
        new AlertDialog.Builder(this)
                .setTitle("Please verify Your Email address")
                .setMessage("Email has been sent to you,Please Click on link to verify it.")
                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth.getInstance().signOut();
                        startActivity(new Intent(SignupActivity.this,SigninActivity.class));
                        finish();
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.

                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}