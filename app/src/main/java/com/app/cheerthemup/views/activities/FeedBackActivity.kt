package com.app.cheerthemup.views.activities

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.cheerthemup.R


class FeedBackActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed_back)


        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "godisking333@gmail.com"))
            intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback About CheerThemUp")
            intent.putExtra(Intent.EXTRA_TEXT, "")
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            //TODO smth
        }
    }
}