package com.app.cheerthemup.views.activities

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.RatingBar

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.app.cheerthemup.R


class RateAndReview : AppCompatActivity() {

    private var rBar: RatingBar? = null
    private var toolbar: Toolbar?=null
    private var btn: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rate_and_review)

        rBar = findViewById<View>(R.id.ratingBar1) as RatingBar
        toolbar=findViewById(R.id.toolbar) as Toolbar

        btn = findViewById<View>(R.id.btnGet) as Button
        toolbar!!.setNavigationOnClickListener(View.OnClickListener { onBackPressed() })
        btn!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                val noofstars = rBar!!.getNumStars()
                val getrating = rBar!!.getRating()

                if (getrating > 3.5) {
                    val uri: Uri = Uri.parse("market://details?id=$packageName")
                    val goToMarket = Intent(Intent.ACTION_VIEW, uri)
                    // To count with Play market backstack, After pressing back button,
                    // to taken back to our applicioatn, we need to add following flags to intent.
                    goToMarket.addFlags(
                            Intent.FLAG_ACTIVITY_NO_HISTORY or
                                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
                    try {
                        startActivity(goToMarket)
                    } catch (e: ActivityNotFoundException) {
                        startActivity(
                                Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("http://play.google.com/store/apps/details?id=$packageName")
                                )
                        )
                    }
                } else {
                    var intent = Intent(applicationContext, ThankYou::class.java)
                    intent.putExtra("rating", getrating)
                    startActivity(intent)
                }
            }
        })


    }
    override fun onResume() {
        super.onResume()

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}