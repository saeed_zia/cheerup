package com.app.cheerthemup.views.activities;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.app.cheerthemup.R;
import com.app.cheerthemup.models.User;
import com.app.cheerthemup.utils.Constants;
import com.app.cheerthemup.utils.Dialogs;
import com.darwin.viola.still.FaceDetectionListener;
import com.darwin.viola.still.Viola;
import com.darwin.viola.still.model.FaceDetectionError;
import com.darwin.viola.still.model.Result;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.annotations.NotNull;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Random;

public class SetupYourProfileActivity extends AppCompatActivity {

    TextInputEditText edtUserName, edtEmail, edtPhone,edtName,edtSurname,edtCity;
    MaterialButton btnSetupProfile, btnSkip;
    ImageView selector,profile;
    boolean uploadStatus,faceDetectStatus;
    //firebase
    private StorageReference mStorageRef;
    private StorageTask mUploadTask;
    private Uri mImageUri;
    Dialogs dialogs=new Dialogs();

    //
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;
    Bitmap bitmap;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_your_profile);

        uploadStatus=false;
        firebaseAuth = FirebaseAuth.getInstance();
        edtUserName = findViewById(R.id.edit_username);
        edtEmail = findViewById(R.id.edit_email);
        edtPhone = findViewById(R.id.edit_phone);
        edtName=findViewById(R.id.edit_name);
        edtSurname=findViewById(R.id.edit_surname);
        edtCity=findViewById(R.id.edit_city);


        btnSkip = findViewById(R.id.btn_skip);
        selector=findViewById(R.id.chooser);
        profile=findViewById(R.id.profile_img);
        //


        mStorageRef = FirebaseStorage.getInstance().getReference("uploads");

//        btnSkip.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                User user = new User("Anonymous", "", "","", false,"",false,false);
//                skipThisStep(user);
//
//
//            }
//        });

        final FaceDetectionListener listener = new FaceDetectionListener() {
            @Override
            public void onFaceDetected(@NotNull Result result) {

                Toast.makeText(getApplicationContext(), "Detected", Toast.LENGTH_SHORT).show();
                 faceDetectStatus=true;
                 profile.setImageBitmap(bitmap);
            }

            @Override
            public void onFaceDetectionFailed(@NotNull FaceDetectionError error, @NotNull String message) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                faceDetectStatus=false;
            }
        };
        // You can do the assignment inside onAttach or onCreate, i.e, before the activity is displayed
        ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            // There are no request codes
                            Intent data = result.getData();
                            try {
                                Uri imageUri = data.getData();
                                 bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), imageUri);

                                mImageUri=imageUri;
                                Viola viola = new Viola(listener);
                                // viola.addAgeClassificationPlugin(context) //optional, available via external dependency
                                viola.detectFace(bitmap);

                            }
                            catch (Exception e){
                                Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });


       selector.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               Intent intent = new Intent();
               intent.setType("image/*");
               intent.setAction(Intent.ACTION_GET_CONTENT);
               someActivityResultLauncher.launch(Intent.createChooser(intent, "Select Picture"));
           }
       });
        String email = firebaseAuth.getCurrentUser().getEmail();
        edtEmail.setText(email);


        btnSetupProfile = findViewById(R.id.btn_setup_my_profile);
        btnSetupProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = firebaseAuth.getCurrentUser().getEmail();
                String userName = edtUserName.getText().toString();
                String phone = edtPhone.getText().toString();
                String name=edtName.getText().toString();
                String surName=edtSurname.getText().toString();
                String city=edtCity.getText().toString();
                surName=givenUsingPlainJava_whenGeneratingRandomStringUnbounded_thenCorrect();

                 if(name.equals("")){
                    edtName.setError("Can't be Empty");
                }
                else if(surName.equals("")){
                    edtSurname.setError("Can't be Empty");
                }
                if(userName.equals("")){
                    edtUserName.setError("Can't be Empty");

                }
                else if(city.equals("")){
                    edtCity.setError("Can't be Empty");
                }
                else if(phone.equals("")){
                    edtPhone.setError("Can't be Empty");
                }
                else{
                    if(faceDetectStatus){
                        dialogs.showProgressDialogWithTitle(SetupYourProfileActivity.this,"Saving your data");
                        uploadFile();
                    }
                }





//                String email = edtEmail.getText().toString();

            }
        });
    }

    private void uploadFile() {
        if (mImageUri != null) {
           // mProgressBar.setVisibility(View.VISIBLE);
            StorageReference fileReference = mStorageRef.child(System.currentTimeMillis()
                    + "." + getFileExtension(mImageUri));
            mUploadTask = fileReference.putFile(mImageUri)

                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //img_url=taskSnapshot.getResult().getMetadata().getReference().getDownloadUrl().toString();
                   fileReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                       @Override
                       public void onSuccess(@NonNull Uri uri) {
                      String  img_url=uri.toString();

                           String email = firebaseAuth.getCurrentUser().getEmail();
                           String userName = edtUserName.getText().toString();
                           String phone = edtPhone.getText().toString();
                           String name=edtName.getText().toString();
                           String surName=edtSurname.getText().toString();
                           String city=edtCity.getText().toString();


//                User user = new User(userName, email, phone, true,firebaseAuth.getCurrentUser().getUid());
//                User user = new User(userName, "Anonymous",email, phone, true,firebaseAuth.getCurrentUser().getUid(),false);
                           User user = new User(userName,"Anonymous",email,phone,true,firebaseAuth.getCurrentUser().getUid(),false,false,name,surName,city,img_url);
                           setupMyProfile(user);


                       }
                   });



                }
            })

                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(SetupYourProfileActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
//                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
//                        @Override
//                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
//                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
//                            mProgressBar.setProgress((int) progress);
//                        }
//                    })
        } else {
            Toast.makeText(this, "No file selected", Toast.LENGTH_SHORT).show();
        }
    }

    public String givenUsingPlainJava_whenGeneratingRandomStringUnbounded_thenCorrect() {
        byte[] array = new byte[7]; // length is bounded by 7
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));

        return generatedString;
    }
    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }
    private void skipThisStep(User user) {
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        String uid = currentUser.getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(uid);
        databaseReference.setValue(user);


    }

    private void setupMyProfile(User user) {


//                    Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            mProgressBar.setProgress(0);
//                        }
//                    }, 1000);


        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        String uid = currentUser.getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(uid);
        databaseReference.setValue(user);
        dialogs.hideDialog();
        startActivity(new Intent(SetupYourProfileActivity.this, DashboardActivity.class));

    }

    @Override
    protected void onStop() {
        super.onStop();

    }
}