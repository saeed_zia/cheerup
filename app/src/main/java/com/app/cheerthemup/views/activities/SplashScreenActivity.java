package com.app.cheerthemup.views.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import com.app.cheerthemup.R;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen2);
        startActivity(new Intent(SplashScreenActivity.this, SigninActivity.class));
        finish();
    }
}